#!/bin/sh

set -e

sed "s/@user@/${FPM_USER}/g; s/@variant@/${FPM_VARIANT}/g" \
    < /usr/local/etc/fpm-pool.conf.tmpl \
    > "/etc/php5/fpm/pool.d/${FPM_USER}.conf"

/etc/init.d/nullmailer start
/usr/sbin/php5-fpm --nodaemonize
