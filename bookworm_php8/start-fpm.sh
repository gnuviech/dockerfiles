#!/bin/sh

set -e

sed "s/@user@/${FPM_USER}/g; s/@variant@/${FPM_VARIANT}/g" \
    < /usr/local/etc/fpm-pool.conf.tmpl \
    > "/etc/php/8.2/fpm/pool.d/${FPM_USER}.conf"

/etc/init.d/nslcd start
/etc/init.d/nullmailer start
mkdir -p /run/php
/usr/sbin/php-fpm8.2 --nodaemonize
