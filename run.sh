#!/bin/sh

set -e
valid_users=$(getent passwd | grep ^usr | cut -d : -f 1)

if [ $# -lt 2 ]; then
    echo "Usage: $0 <dist> [<variant>] <user>"
    echo
    echo "<dist> is one of wheezy, jessie, stretch, buster"
    echo "<variant> is one of mysql or pgsql"
    echo "<user> is a user name defined in ldap and on file"
    echo
    for u in $valid_users; do echo $u; done | xargs -n 10 echo
    exit 1
fi

if [ $# -eq 3 ]; then
    dist=$1
    username="$3"
    variant="-$2"
else
    dist=$1
    username="$2"
    variant=""
fi

case $dist in
    wheezy|jessie)
        image=gnuviech/${dist}_php5${variant}
        ;;
    stretch|buster)
        image=gnuviech/${dist}_php7${variant}
        ;;
    *)
        echo "Unknown distribution $dist"
        exit 2
esac

for u in $valid_users; do
    if [ "$u" = "${username}" ]; then
        choosen_user=$u
    fi
done

if [ -z "$choosen_user" ]; then
    echo "Invalid user ${username}"
    exit 3
fi

docker run \
    --volume-driver=nfs --net=host --rm --detach \
    -v "file/web/$choosen_user:/srv" \
    -v "/var/run/php-fpm-docker:/var/run/php-fpm-docker" \
    -v "/var/log/php-fpm-docker:/var/log/php-fpm-docker" \
    -e "FPM_USER=$choosen_user" \
    -e "FPM_VARIANT=${dist}${variant}" \
    --name "${choosen_user}_${dist}${variant}" \
    "$image"
