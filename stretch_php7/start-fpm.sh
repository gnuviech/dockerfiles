#!/bin/sh

set -e

sed "s/@user@/${FPM_USER}/g; s/@variant@/${FPM_VARIANT}/g" \
    < /usr/local/etc/fpm-pool.conf.tmpl \
    > "/etc/php/7.0/fpm/pool.d/${FPM_USER}.conf"

/etc/init.d/nullmailer start
mkdir -p /run/php
/usr/sbin/php-fpm7.0 --nodaemonize
