#!/bin/sh

set -e

for dist in jessie_php5 stretch_php7 buster_php7 bullseye_php7 bookworm_php8; do
    docker build --pull --no-cache -t gnuviech/${dist}-base ${dist} -f ${dist}/Dockerfile-base
    docker build -t gnuviech/${dist} ${dist} -f ${dist}/Dockerfile
    docker build -t gnuviech/${dist}-mysql ${dist} -f ${dist}/Dockerfile-mysql
    docker build -t gnuviech/${dist}-pgsql ${dist} -f ${dist}/Dockerfile-pgsql
done

docker image prune -f
